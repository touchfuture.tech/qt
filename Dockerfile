FROM golang:alpine

WORKDIR  /go/src/webserver/

COPY . .

RUN go get -v
RUN go install

EXPOSE 8080

CMD ["/go/bin/webserver"]